package com.example.mb.torrentplayer;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;


public class AppDelegate extends Application {
    public static final String TAG = "com.example.mb.torrentplayer";

    AppService appService;
    boolean appServiceBound = false;




    private static AppDelegate instance;
    public AppDelegate() {
        instance = this;
    }
    public static Context getContext() {
        return instance;
    }





    private ServiceConnection appServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AppService.LocalBinder binder = (AppService.LocalBinder) service;
            appService = binder.getService();
            appServiceBound = true;

            Intent i = new Intent(getContext(), MainActivity.class);
            i.putExtra("text", "About");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(i);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            appServiceBound = false;
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "app started");

        Intent i = new Intent(this, AppService.class);
        bindService(i, appServiceConnection, Context.BIND_AUTO_CREATE);
    }


    public String getCurrentTime() {
        if (appServiceBound == false) return "none";

        return appService.getCurrentTime();
    }
}
