package com.example.mb.torrentplayer;



import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;




public class AppService extends Service {
    private static final String TAG = "com.example.mb.torrentplayer";

    private final IBinder appServiceBinder = new LocalBinder();


    public AppService() {
    }

    public String getCurrentTime() {
        return "GOT TIME FROM AppService";
    }



    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind");
        return appServiceBinder;
    }

   public class LocalBinder extends Binder {
        AppService getService() {
            return AppService.this;
        }
    }
}


