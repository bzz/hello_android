package com.example.mb.torrentplayer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AppSplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_splash_screen);
    }
}
