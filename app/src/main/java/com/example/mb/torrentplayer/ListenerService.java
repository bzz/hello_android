package com.example.mb.torrentplayer;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


public class ListenerService extends IntentService {
    private static final String TAG = "com.example.mb.torrentplayer";

    public static volatile boolean shouldContinue = true;

    public  ListenerService () {
        super("ListenerService");
    }

    NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
    private static final int uniqueID = 33222;


    @Override
    protected void onHandleIntent(Intent intent) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                for (;;) {

                    if (shouldContinue == false) {
                        stopSelf();
                        return;
                    }

                    long futureTime = System.currentTimeMillis() + 2000;
                    while (System.currentTimeMillis() < futureTime) {

                        synchronized (this) {
                            try {
                                Log.i(TAG, "working...");
                                wait(futureTime - System.currentTimeMillis());
                            } catch (Exception e) {}
                        }
                    }
                }
            }
        };

        Thread listenerThread = new Thread(r);
        listenerThread.start();

///        notificationShow();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);;
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }



    public void notificationShow() {
        notification.setAutoCancel(true);
        notification.setSmallIcon(R.drawable.notif_icon);
        notification.setTicker("Ticker text");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("title!");
        notification.setContentText("text!");
        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());
    }
}
